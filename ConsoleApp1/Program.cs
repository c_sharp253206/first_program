﻿// Test protected #2 Conflict in this line

Console.Write("Input size: "); // message for user
int size = 6; // default
string s = Console.ReadLine()!; // read input

// validate user's string input
if (s.Length == 0) 
{
    Console.WriteLine("Invalid input. It was set default size value: 6");
}
else 
{
    size = int.Parse(s);
    Console.WriteLine("Your choise(size): " + size);
}

Console.WriteLine("Input symbol for paint: "); // message for user
char c = 'x'; // default symbol
s = Console.ReadLine(); // read input

// validate user's char input
if (s.Length == 0) 
{
    Console.WriteLine("Invalid input. It was set default symbol value: x");
}
else 
{
    c = s[0];
    Console.WriteLine("Your choise(symbol): " + c);
}

// set color
Console.ForegroundColor = ConsoleColor.Red;


// top
for (int i = 0; i < size; i++)
{
    Console.Write("x");
}
Console.WriteLine();

// main
for (int i = 0; i < size - 2; i++)
{
    Console.Write("x");
    for (int j = 0; j < size - 2; j++)
    {
        Console.Write(" ");
    }
    Console.Write("x");
    Console.WriteLine();
}

// bottom
for (int i = 0; i < size; i++)
{
    Console.Write("x");
}

Console.WriteLine();
Console.ResetColor();
Console.WriteLine("Program ends");

// Add some more for test new branch and megre its